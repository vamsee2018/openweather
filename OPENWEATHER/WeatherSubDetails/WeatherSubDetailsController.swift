//
//  WeatherSubDetailsController.swift
//  OPENWEATHER
//
//  Created by VAMC  on 28/02/22.
//

import UIKit

class WeatherSubDetailsController: UIViewController {
    var weatherReport: WeatherData?

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var weatherResponse: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var temp: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "City Weather Details"
        
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: weatherReport?.name, style: .plain, target: nil, action: nil)
        
        self.feelsLikeLabel.text = weatherReport?.main?.feelsLike?.string
        self.weatherResponse.text = weatherReport?.weather?.first?.main ?? ""
        self.descriptionLabel.text = weatherReport?.weather?.first?.weatherDescription ?? ""
        self.temp.text = weatherReport?.main?.temp?.string
    }
}
