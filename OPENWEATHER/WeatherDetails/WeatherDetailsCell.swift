//
//  WeatherDetailsCell.swift
//  OPENWEATHER
//
//  Created by VAMC  on 28/02/22.
//

import UIKit

class WeatherDetailsCell: UITableViewCell {
    
    @IBOutlet weak var weatherTitle: UILabel!
    @IBOutlet weak var weatherResponse: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
