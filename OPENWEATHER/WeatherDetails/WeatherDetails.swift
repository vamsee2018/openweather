//
//  WeatherDetails.swift
//  OPENWEATHER
//
//  Created by VAMC  on 28/02/22.
//

import UIKit

enum WeatherConstants {
    static let weatherDetailsCell = "WeatherDetailsCell"
}

class WeatherDetailsController: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    var weatherReport: WeatherData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Temperature & Weather"

        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            self.navigationItem.backBarButtonItem = UIBarButtonItem(
                title: appDelegate.detailTitle, style: .plain, target: nil, action: nil)

            self.tableView?.reloadData()
        }
    }
    
}

extension WeatherDetailsController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: WeatherConstants.weatherDetailsCell,
            for: indexPath) as? WeatherDetailsCell
        guard let weatherResponse = weatherReport else {
            return cell ?? UITableViewCell()
        }
        
        cell?.selectionStyle = .none

        switch indexPath.row {
        case 0:
            cell?.weatherTitle.text = "Temperature"
            cell?.weatherResponse.text = weatherResponse.main?.temp?.string
        case 1:
            cell?.weatherTitle.text = "Weather"
            cell?.weatherResponse.text = weatherResponse.weather?.first?.main ?? ""
        default:
            break
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigateToWeatherSubDetails()
    }
    
    private func navigateToWeatherSubDetails() {
        DispatchQueue.main.async { [self] in
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WeatherSubDetailsController") as? WeatherSubDetailsController
            vc?.weatherReport = self.weatherReport
        self.navigationController?.pushViewController(vc!, animated: true)
        }
    }

}

extension LosslessStringConvertible {
    var string: String { .init(self) }
}
