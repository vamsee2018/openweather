
import Foundation

final class WeatherServiceWorker {
    
    func getWeatherReport(city: String, callback: @escaping (Result<WeatherData?, APIError>) -> Void) {
        NetWorkManager.shared.getWeatherReport(city: city, request: WeatherService().path) { result in
            switch result {
            case .success(let wData):
                guard let data = wData else {
                    return
                }
                callback(.success(data))
            case .failure(let error):
                callback(.failure(.invalidData))
                print(error)
            }
        }
        
    }
}

class WeatherService: BaseService {
    override var path: String {
        return APIConfig.baseURL
    }

    override var responseParser: ResponseParser? {
        return GenericParser<WeatherData>()
    }
}

