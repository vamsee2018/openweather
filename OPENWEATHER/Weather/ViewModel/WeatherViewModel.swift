import Foundation

class WeatherViewModel {
    var weatherReport: WeatherData?
    var updateWeatherUI: ((APIError?) -> Void)?

    func getWeather(cityName: String) {
        WeatherServiceWorker().getWeatherReport(city: cityName) {[weak self] (result) in
            switch result {
            case .success(let weather):
                guard let data = weather else {
                    return
                }
                self?.weatherReport = data
                self?.updateWeatherUI?(nil)
            case .failure(_):
                self?.updateWeatherUI?(APIError.invalidData)
            }
        }
    }
}
