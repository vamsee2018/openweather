//
//  ViewController.swift
//  OPENWEATHER
//
//  Created by VAMC  on 27/02/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lookUpButton: UIButton!
    @IBOutlet weak var cityTextField: UITextField!
    
    let viewModel = WeatherViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "OPEN WEATHER!"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        initialSetUp()
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func initialSetUp() {
        lookUpButton?.backgroundColor = .clear
        lookUpButton?.layer.cornerRadius = 5
        lookUpButton?.layer.borderWidth = 1
        lookUpButton?.layer.borderColor = UIColor.black.cgColor
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = ""
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == true {
            textField.placeholder = "Enter City Name"
            return false
        } else {
            return true
        }

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("should return called")
        return true
      }
    }

extension ViewController {
    @IBAction func lookUpButtonTapped(_ sender: UIButton) {
        viewModel.getWeather(cityName: cityTextField.text ?? "")
        viewModel.updateWeatherUI = {[weak self] (status) in
            
            if status == nil {
                DispatchQueue.main.async { [self] in
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.detailTitle = self?.cityTextField?.text ?? ""
                }
                
                    self?.navigateToWeatherDetails()
            } else {
                self?.handleError()
            }
        }
    }
    
    private func navigateToWeatherDetails() {
        DispatchQueue.main.async { [self] in
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WeatherDetailsController") as? WeatherDetailsController
            vc?.weatherReport = self.viewModel.weatherReport
        self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    private func handleError() {
        DispatchQueue.main.async {
               let alert = UIAlertController(title: "OOPS..!", message: "Please enter a valid city name!", preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
               self.present(alert, animated: true, completion: nil)
        }
    }
}

