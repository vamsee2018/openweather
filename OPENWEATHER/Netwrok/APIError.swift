import Foundation

enum APIError: Error {
    case SomeRealisticErrorThing
    case invalidId
    case unableToComplete
    case invalidData
    
    var localizedDescription: String {
        switch self {
        case .SomeRealisticErrorThing: return "Some Realistic Error Thing"
        case .invalidId: return "Invalid Id"
        case .unableToComplete: return "Unable To Complete"
        case .invalidData: return "Invalid Data"
        }
    }
}
