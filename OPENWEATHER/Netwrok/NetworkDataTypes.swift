
import Foundation

public typealias JsonDataType = [String: Any]

/// Define parameters to pass along with the request and how
/// they are encapsulated into the http request itself.
public typealias RequestParams = [String: Any]

/// Define the Type for HTTP headers that can be added to the RequestBuilder
public typealias AKHTTPHeaders = [String: String]

/// Define the download response
/// - temporaryURL: url where the data been stored temporarily.
/// - response: HTTP URL response
public typealias DownloadResponse = (
    temporaryURL: URL?,
    response: HTTPURLResponse?
)

/// This define the type of HTTP method used to perform the request
///
/// - post: POST method
/// - put: PUT method
/// - get: GET method
/// - delete: DELETE method
/// - patch: PATCH method
public enum AKHTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

/// Generally used HTTP header keys
public enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

/// Generally used HTTP header values
enum ContentType: String {
    case json = "application/json"
}

/// Expected data type from a webservice
public enum ResponseType {
    case JSON
    case Data
    case XML
}

/// Define the type for a result from webservice
/// - success: if service is successful use this case and pass along the data received.
/// - error: if error returned then use this case
public enum AKResult<T> {
    case success(T)
    case error(Error?)
    /// Returns `true` if the result is a success, `false` otherwise.
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .error:
            return false
        }
    }
    /// Returns `true` if the result is a failure, `false` otherwise.
    public var isError: Bool {
        return !isSuccess
    }
    /// Returns the associated value if the result is a success, `nil` otherwise.
    public var value: T? {
        switch self {
        case .success(let value):
            return value
        case .error:
            return nil
        }
    }
    /// Returns the associated error value if the result is a failure, `nil` otherwise.
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .error(let error):
            return error
        }
    }
}

extension AKResult {
    /// - parameter value: The closure to execute and create the result for.
    public init(temp: () throws -> T) {
        do {
            self = try .success(temp())
        } catch {
            self = .error(error)
        }
    }
    /// Returns the success value, or throws the failure error.
    public func unwrap() throws -> T {
        switch self {
        case .success(let value):
            return value
        case .error(let error):
            throw error ?? NetworkErrors.unknown
        }
    }
}

/// Define the type for a result from a download service
/// - success: if service is successful then use this case
/// - error: if error returned then use this case
public enum DownloadResult<T> {
    case success(DownloadResponse)
    case error(Error?)
}

/// Defining network errors
public enum NetworkErrors: Error {
    case unknown
    case badRequest
    case emptyData
    case invalidResponse
    case urlError(URLError?)
}
