
import Foundation

typealias JSONDictionary = [String:Any]

class NetWorkManager {
    static let shared = NetWorkManager()
    let session = URLSession.shared
    
    func getWeatherReport(city: String, request: String, callback: @escaping (Result<WeatherData?, APIError>) -> Void) {
        let queryString = request
        let urlString = ("\(queryString)&q=\(city)")
        guard let requestUrl = URL(string: urlString) else {
            callback(.failure(APIError.invalidData))
            return
        }
        
        guard URL(string: queryString) != nil else {
            callback(.failure(APIError.invalidId))
            return
        }
        
        session.dataTask(with: requestUrl) { (data, response, error) in
            if error != nil {
                callback(.failure(APIError.unableToComplete))
                return
            }
            
            guard let data = data else {
                callback(.failure(APIError.invalidData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let weather = try decoder.decode(WeatherData.self, from: data)
                callback(.success(weather))
            } catch {
                callback(.failure(APIError.invalidData))
            }
            
        }.resume()
    }
}
   


public protocol RequestBuilder {
    var path: String { get }
    var baseURL: URL? { get }
    var responseParser: ResponseParser? { get }
}


class BaseService: RequestBuilder {
    var path: String {
        return ""
    }
    
    var baseURL: URL? {
        return URL(string:APIConfig.baseURL)
    }
    
    var responseParser: ResponseParser? {
        return ResponseParserJSON()
    }
}

open class ResponseParserJSON: ResponseParser {
    public init() {
        // intentionally left blank
    }
    public func parse(_ data: Data) -> Any? {
        let recipient = data.parseJSON()
        return recipient
    }
}


public protocol ResponseParser {
    func parse(_ data: Data) throws -> Any?
}


extension Data {
    public func decode<T: Decodable>() throws -> T {
        return try JSONDecoder().decode(T.self, from: self)
    }

    public func parseJSON() -> Any? {
        do {
            let responseBody = try JSONSerialization.jsonObject(with: self, options: .allowFragments)
            return responseBody
        } catch let error {
            return error
        }
    }
}


open class GenericParser<T: Decodable>: ResponseParser {

    public init() {
        // left intentionally blank
    }

    public func parse(_ data: Data) throws -> Any? {
        do {
            let recipient = try data.decode() as T
            return recipient
        } catch {
            throw error
        }
    }
}

